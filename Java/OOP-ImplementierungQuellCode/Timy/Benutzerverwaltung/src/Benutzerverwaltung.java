import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction;

public class Benutzerverwaltung {

    public static void main(String[] args) {

        ArrayList<Benutzer> benutzerliste = new ArrayList<Benutzer>();

        int auswahl;

        do {
            auswahl = menu();
            switch (auswahl) {
                case 1:
                    // bv.benutzerAnzeigen();
                    System.out.println(benutzerliste);
                    break;
                case 2:
                    benutzerErfassen(benutzerliste);

                    break;
                case 3:
                    // benutzerLoeschen();
                    break;
                case 4:
                    System.exit(0);
                default:
                    System.err.println("\nFalsche Eingabe.\n");
            }
        } while (true);

    }

    public static int menu() {

        int selection;
        Scanner input = new Scanner(System.in);

        /***************************************************/

        System.out.println("----------------------------");
        System.out.println("     Benutzerverwaltung     ");
        System.out.println("----------------------------\n");
        System.out.println("1 - Benutzer anzeigen");
        System.out.println("2 - Benutzer erfassen");
        System.out.println("3 - Benutzer loeschen");
        System.out.println("4 - Ende");

        System.out.print("\nEingabe:  ");
        selection = input.nextInt();
        return selection;
    }

    public static void benutzerErfassen(ArrayList<Benutzer> benutzerliste) {
        // ID, Vorname, Nachname, Geburtsjahr von Tastatur lesen
        int uin;
        String fname;
        String lname;
        int birthY;

        Scanner input = new Scanner(System.in);

        System.out.println("Bitte geben sie die User ID ein: ");
        uin = input.nextInt();
        input.nextLine();
        System.out.println("Bitte geben sie den Vornamen ein: ");
        fname = input.nextLine();
        System.out.println("Bitte geben sie den Nachnamen ein: ");
        lname = input.nextLine();
        System.out.println("Bitte geben sie das Geburtsdatum ein: ");
        birthY = input.nextInt();

        Benutzer b1 = new Benutzer(uin, fname, lname, birthY);
        benutzerliste.add(b1);
    }

}