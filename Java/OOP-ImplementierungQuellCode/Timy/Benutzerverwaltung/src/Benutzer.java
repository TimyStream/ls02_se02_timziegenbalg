import java.text.SimpleDateFormat;
import java.util.Date;

public class Benutzer {

    private int userIdentN;
    private String fname;
    private String lname;
    private int birthY;

    public Benutzer() {

    }

    /**
     * Konstruktor mit Parametern
     * 
     * @param userIdentN
     * @param fname
     * @param lname
     * @param birthY
     */
    public Benutzer(int userIdentN, String fname, String lname, int birthY) {
        this.userIdentN = userIdentN;
        this.fname = fname;
        this.lname = lname;
        this.birthY = birthY;
    }

    public void setUserIdentN(int userIdentN) {
        this.userIdentN = userIdentN;
    }

    public int getUserIdentN() {
        return userIdentN;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getFname() {
        return fname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getLname() {
        return lname;
    }

    public void setBirthY(int birthY) {
        this.birthY = birthY;
    }

    public int getBirthY() {
        return birthY;
    }

    /**
     * Gibt das Alter der Person auf Basis des aktuellen jahrs zurück.
     * 
     * @return age
     */
    public int getAge() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
        Date date = new Date();
        int aYear = Integer.parseInt(formatter.format(date));

        int age = aYear - birthY;

        return age;
    }

    @Override
    public String toString() {
        return "ID: " + this.userIdentN + " Nachname: " + this.lname + " Vorname: " + this.fname;
    }
}