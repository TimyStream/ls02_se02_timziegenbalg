public class StarTrek {
    public static void main(String[] args) throws Exception {
        // Create Util Class Object
        Utils u = new Utils();

        // #region Create Ladungen

        // Raumschiff 1
        Ladung l1 = new Ladung("Ferengi Schneckensaft", 200);
        Ladung l2 = new Ladung("Bat'leth Klingonen Schwert", 200);
        // Raumschiff 2
        Ladung l3 = new Ladung("Borg-Schrott", 5);
        Ladung l4 = new Ladung("Rote Materie", 2);
        Ladung l5 = new Ladung("Plasma-Waffe", 50);
        // Raumschiff 3
        Ladung l6 = new Ladung("Forschungssonde", 35);
        Ladung l7 = new Ladung("Photonentorpedo", 3);

        // #endregion

        // #region Create Raumschiffe
        Raumschiff klingonen = new Raumschiff("IKS Hegh'ta", 1, 100, 100, 100, 100, 2);
        Raumschiff romulaner = new Raumschiff("IRW Khazara", 2, 100, 100, 100, 100, 2);
        Raumschiff vulkanier = new Raumschiff("Ni'Var", 0, 80, 80, 50, 100, 5);
        // #endregion

        // #region Klingonen Schiff
        klingonen.addLoad(l1);
        klingonen.addLoad(l2);
        klingonen.zustand();
        // #endregion
        u.cL();
        // #region Romulaner Schiff
        romulaner.addLoad(l3);
        romulaner.addLoad(l4);
        romulaner.addLoad(l5);
        romulaner.zustand();
        // #endregion
        u.cL();
        // #region Vulkanier Schiff
        vulkanier.addLoad(l6);
        vulkanier.addLoad(l7);
        vulkanier.zustand();
        // #endregion
    }
}
