import java.util.ArrayList;

public class ArrayListBeispiele {
    public static void main(String[] args) throws Exception {

        /*
         * ArrayList<String> namenliste = new ArrayList<String>();
         * 
         * namenliste.add("Max"); // Index 0 namenliste.add("Alex"); // Index 1
         * namenliste.add("Anna"); // Index 2 System.out.println(namenliste);
         * 
         * namenliste.remove(1);
         * 
         * System.out.println(namenliste);
         */

        //////////////////////////////////////////////////////////////////////

        /*
         * ArrayList<Buch> buchliste = new ArrayList<Buch>();
         * 
         * Buch b1 = new Buch("Romeo und Julia", 20.00); Buch b2 = new Buch("Egghead",
         * 15.00);
         * 
         * buchliste.add(b1); // Index 0 buchliste.add(b2); // Index 1
         * 
         * System.out.println(buchliste);
         */

        ArrayList<Person> pl = new ArrayList<Person>();

        Person p1 = new Person("Alexander", 19);
        Person p2 = new Person("Nico", 16);

        pl.add(p1);
        pl.add(p2);

        System.out.println(pl);

        Person p3 = new Person("Jona", 18);

    }
}
