public class Person {

    private String name;
    private int alter;

    /**
     * Konstruktor für die Personen Klasse
     * 
     * @param name
     * @param alter
     */
    public Person(String name, int alter) {
        this.name = name;
        this.alter = alter;
    }

    /**
     * Setter for name Variable
     * 
     * @param name Name der Person
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter for name Variable
     * 
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Setter for alter Variable
     * 
     * @param alter alter der Person
     */
    public void setAlter(int alter) {
        this.alter = alter;
    }

    /**
     * Getter for alter Variable
     * 
     * @return alter
     */
    public int getAlter() {
        return alter;
    }

    /**
     * Wir überschreiben die Standard Funktion, damit unsere Arraylist ordentlich
     * angezeigt wird statt person@XYZXX
     */
    @Override
    public String toString() {
        return "[" + this.name + " , " + this.alter + "]";
    }

}
